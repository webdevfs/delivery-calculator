1. Install dependencies:

```sh
composer install
```

2. Run the tests:

2.1. Make sure the run-tests.sh has executable flag:

```sh
chmod +x run-tests.sh
```

2.2. Run the tests:

```sh
./run-tests.sh
```