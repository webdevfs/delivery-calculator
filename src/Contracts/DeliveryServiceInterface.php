<?php

namespace Webdevfs\DeliveryCalculator\Contracts;

interface DeliveryServiceInterface
{
    public function calculateDelivery(): array;
}
