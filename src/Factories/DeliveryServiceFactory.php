<?php

namespace Webdevfs\DeliveryCalculator\Factories;

use Webdevfs\DeliveryCalculator\Contracts\DeliveryServiceInterface;
use Webdevfs\DeliveryCalculator\Services\FastDelivery;
use Webdevfs\DeliveryCalculator\Services\SlowDelivery;

class DeliveryServiceFactory
{
    public static function create($type, $base_url, $sourceKladr, $targetKladr, $weight): DeliveryServiceInterface
    {
        switch ($type) {
            case 'fast':
                return new FastDelivery($base_url, $sourceKladr, $targetKladr, $weight);
            case 'slow':
                return new SlowDelivery($base_url, $sourceKladr, $targetKladr, $weight);
            default:
                throw new Exception("Unknown delivery service type: $type");
        }
    }
}
