<?php

namespace Webdevfs\DeliveryCalculator\Services;

use Webdevfs\DeliveryCalculator\Contracts\DeliveryServiceInterface;

abstract class AbstractDeliveryService implements DeliveryServiceInterface
{
    protected $base_url;
    protected $sourceKladr;
    protected $targetKladr;
    protected $weight;

    public function __construct($base_url, $sourceKladr, $targetKladr, $weight)
    {
        $this->base_url = $base_url;
        $this->sourceKladr = $sourceKladr;
        $this->targetKladr = $targetKladr;
        $this->weight = $weight;
    }
}
