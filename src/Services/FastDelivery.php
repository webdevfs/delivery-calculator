<?php

namespace Webdevfs\DeliveryCalculator\Services;

class FastDelivery extends AbstractDeliveryService
{
    public function calculateDelivery(): array
    {
        // Логика расчета для "Быстрой доставки"
        // Эмуляция получения данных от службы доставки
        $response = json_decode('{
            "price": 500,
            "period": 3,
            "error": ""
        }');

        return [
            'price' => $response->price,
            'date'  => date('Y-m-d', strtotime("+$response->period days")),
            'error' => $response->error
        ];
    }
}
