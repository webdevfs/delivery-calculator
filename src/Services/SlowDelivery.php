<?php

namespace Webdevfs\DeliveryCalculator\Services;

class SlowDelivery extends AbstractDeliveryService
{
    public const BASE_PRICE = 150;

    public function calculateDelivery(): array
    {
        // Логика расчета для "Медленной доставки"
        // Эмуляция получения данных от службы доставки
        $response = json_decode('{
            "coefficient": 1.5,
            "date": "2023-10-10",
            "error": ""
        }');

        return [
            'price' => self::BASE_PRICE * $response->coefficient,
            'date'  => $response->date,
            'error' => $response->error
        ];
    }
}