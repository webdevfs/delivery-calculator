<?php

use PHPUnit\Framework\TestCase;
use Webdevfs\DeliveryCalculator\Services\FastDelivery;

class FastDeliveryTest extends TestCase
{
    public function testCalculateDelivery()
    {
        $fastDelivery = new FastDelivery('https://fastdelivery.example.com', 'sourceKladr', 'targetKladr', 5);

        $result = $fastDelivery->calculateDelivery();

        $this->assertIsArray($result);
        $this->assertArrayHasKey('price', $result);
        $this->assertArrayHasKey('date', $result);
        $this->assertArrayHasKey('error', $result);
    }
}
