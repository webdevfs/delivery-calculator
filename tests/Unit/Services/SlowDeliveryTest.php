<?php

use PHPUnit\Framework\TestCase;
use Webdevfs\DeliveryCalculator\Services\SlowDelivery;

class SlowDeliveryTest extends TestCase
{
    public function testCalculateDelivery()
    {
        $slowDelivery = new SlowDelivery('https://slowdelivery.example.com', 'sourceKladr', 'targetKladr', 5);

        $result = $slowDelivery->calculateDelivery();

        $this->assertIsArray($result);
        $this->assertArrayHasKey('price', $result);
        $this->assertArrayHasKey('date', $result);
        $this->assertArrayHasKey('error', $result);
    }
}
